import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {



  currentUser={}
 log_out_see :boolean = false;
 nav_see: boolean = false;
  constructor(   private router: Router) { }
  

  ngOnInit() {
    // this.currentUser = {
    //   profile: {}
    // };
    // this.userService.currentUser.subscribe(
    //   (userData) => {
    //     this.currentUser = userData;
    //   }
    // );
  }
  see_log_out(): void {
    this.log_out_see = !this.log_out_see;
  }
  see_nav(): void {
    this.nav_see = !this.nav_see;
  }
}
