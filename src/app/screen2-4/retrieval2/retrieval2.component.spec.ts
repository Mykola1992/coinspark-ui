import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Retrieval2Component } from './retrieval2.component';

describe('Retrieval2Component', () => {
  let component: Retrieval2Component;
  let fixture: ComponentFixture<Retrieval2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Retrieval2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Retrieval2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
