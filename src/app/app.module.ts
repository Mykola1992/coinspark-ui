
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UserService } from './user.service';
import { LoginComponent } from './screen2-4/login/login.component';
import { ListComponent } from './list/list.component';
import { RetrievalComponent } from './screen2-4/retrieval/retrieval.component';
import { Retrieval2Component } from './screen2-4/retrieval2/retrieval2.component';
import { AdminComponent } from './nav/admin/admin.component';
import { InvestorsComponent } from './screen7-13/investors/investors.component';
import { Screen8Component } from './screen7-13/screen8/screen8.component';
import { IndividualInvestorComponent } from './screen7-13/individual-investor/individual-investor.component';
import { InvestorVerificationComponent } from './screen7-13/investor-verification/investor-verification.component';
import { Table13Component } from './screen7-13/table13/table13.component';
import { NewQuestionComponent } from './screen7-13/new-question/new-question.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { NavComponent } from './user/nav/nav.component';
import { SignupComponent } from './user/signup/signup.component';
import { StartedComponent } from './user/started/started.component';
import { VerificationStartedComponent } from './user/verification-started/verification-started.component';
import { TransactionHistoryComponent } from './user/transaction-history/transaction-history.component';
import { FaqComponent } from './user/faq/faq.component';
import { AndStepComponent } from './user/and-step/and-step.component';
import { DataEntryComponent } from './user/data-entry/data-entry.component';
import { BeySparkComponent } from './user/bey-spark/bey-spark.component';
import { FundsTransferComponent } from './user/funds-transfer/funds-transfer.component';
import { FundsFundsReceivedComponent } from './user/funds-funds-received/funds-funds-received.component';
import { VerificationPendingComponent } from './user/verification-pending/verification-pending.component';
import { Verification2ndStepComponent } from './user/verification2nd-step/verification2nd-step.component';
import { AccountConributionComponent } from './user/account-conribution/account-conribution.component';
import { UnsuccessfulComponent } from './user/unsuccessful/unsuccessful.component';
import { BlockedUserComponent } from './user/blocked-user/blocked-user.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { SendConfirmationComponent } from './user/send-confirmation/send-confirmation.component';
import { Dashboard5cComponent } from './screen2-4/dashboard5c/dashboard5c.component';
import { HeaderComponent } from './layout/header/header.component';
import { MenuComponent } from './layout/menu/menu.component';
import { ItemComponent } from './layout/item/item.component';
import { OmeComponent } from './ome/ome.component';





const routes = [
  {path : '', component: OmeComponent},
  {path : 'login', component : LoginComponent },
  {path : 'retrieval', component : RetrievalComponent },
  {path : 'retrieval2', component : Retrieval2Component },
  {path : 'nav/admin', component : AdminComponent },
  {path : 'investor', component : InvestorsComponent },
  {path : 'screen8', component : Screen8Component },
   {path : 'investor/invalid', component : IndividualInvestorComponent  },
   {path : 'verification', component : InvestorVerificationComponent  },
   {path : 'table', component : Table13Component  },
   {path : 'new/question', component : NewQuestionComponent  },
   {path : 'registration', component : RegistrationComponent  },
   {path : 'user/nav', component : NavComponent  },
   {path : 'user/login', component : SignupComponent  },
   {path : 'user/started', component : StartedComponent  },
   {path : 'user/verification/started', component : StartedComponent  },
   {path : 'user/transaction/history', component : TransactionHistoryComponent  },
   {path : 'user/faq', component : FaqComponent  },
   {path : 'user/and/step', component : AndStepComponent  },
   {path : 'user/data/entry', component : DataEntryComponent  },
   {path : 'user/bay/spark', component : BeySparkComponent  },
   {path : 'user/funds/transfer', component : FundsTransferComponent  },
   {path : 'user/funds/received', component : FundsFundsReceivedComponent  },
   {path : 'user/verification/pending', component : VerificationPendingComponent  },
   {path : 'user/verification2ndstep', component : Verification2ndStepComponent  },
   {path : 'user/account/conribution', component : AccountConributionComponent  },
   {path : 'user/unsuccessful', component : UnsuccessfulComponent  },
   {path : 'user/block', component : BlockedUserComponent  },
   {path : 'user/dashboard', component : DashboardComponent  },
   {path : 'user/sendconfirmation', component : SendConfirmationComponent  },
   {path : 'dashboard', component : Dashboard5cComponent  },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListComponent,
    RetrievalComponent,
    Retrieval2Component,
    AdminComponent,
    InvestorsComponent,
    Screen8Component,
    IndividualInvestorComponent,
    InvestorVerificationComponent,
    Table13Component,
    NewQuestionComponent,
    RegistrationComponent,
    NavComponent,
    SignupComponent,
    StartedComponent,
    VerificationStartedComponent,
    TransactionHistoryComponent,
    FaqComponent,
    AndStepComponent,
    DataEntryComponent,
    BeySparkComponent,
    FundsTransferComponent,
    FundsFundsReceivedComponent,
    VerificationPendingComponent,
    Verification2ndStepComponent,
    AccountConributionComponent,
    UnsuccessfulComponent,
    BlockedUserComponent,
    DashboardComponent,
    SendConfirmationComponent,
    Dashboard5cComponent,
    HeaderComponent,
    MenuComponent,
    ItemComponent,
    OmeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
