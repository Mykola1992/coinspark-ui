import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FundsFundsReceivedComponent } from './funds-funds-received.component';

describe('FundsFundsReceivedComponent', () => {
  let component: FundsFundsReceivedComponent;
  let fixture: ComponentFixture<FundsFundsReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundsFundsReceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FundsFundsReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
