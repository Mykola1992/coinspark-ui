import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verification-started',
  templateUrl: './verification-started.component.html',
  styleUrls: ['./verification-started.component.scss']
})
export class VerificationStartedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
