import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationStartedComponent } from './verification-started.component';

describe('VerificationStartedComponent', () => {
  let component: VerificationStartedComponent;
  let fixture: ComponentFixture<VerificationStartedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationStartedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationStartedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
