import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountConributionComponent } from './account-conribution.component';

describe('AccountConributionComponent', () => {
  let component: AccountConributionComponent;
  let fixture: ComponentFixture<AccountConributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountConributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountConributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
