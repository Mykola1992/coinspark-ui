import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeySparkComponent } from './bey-spark.component';

describe('BeySparkComponent', () => {
  let component: BeySparkComponent;
  let fixture: ComponentFixture<BeySparkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeySparkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeySparkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
