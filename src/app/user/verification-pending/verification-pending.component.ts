import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verification-pending',
  templateUrl: './verification-pending.component.html',
  styleUrls: ['./verification-pending.component.scss']
})
export class VerificationPendingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
