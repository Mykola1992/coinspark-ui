import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestorVerificationComponent } from './investor-verification.component';

describe('InvestorVerificationComponent', () => {
  let component: InvestorVerificationComponent;
  let fixture: ComponentFixture<InvestorVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestorVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestorVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
