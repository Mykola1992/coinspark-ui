import { Component, OnInit,Input, OnChanges } from '@angular/core';

// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { ActivatedRoute, Router, Params } from '@angular/router';
// import { UserService, SettingsService } from '../../shared';

@Component({
  selector: 'app-investors',
  templateUrl: './investors.component.html',
  styleUrls: ['././scss/investors.component.scss', './scss/right-menu.scss', './scss/color-status.scss' , './scss/tablet.scss' , '././scss/slice_box.scss']
})
export class InvestorsComponent implements OnInit {

  profiless: any = [{
    firstName: "Petro",
    lastName : "Petrov",
    Tel: "+ 12 097 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My pasport.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'petrov@gmail.com',
    status: 'NotVerified'
  }, {
    firstName: "Ivan",
    lastName : "Repa",
    Tel: "+ 99 097 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My gg.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '213 643',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'Ivan@gmail.com',
    status: 'Confirmed',
    Confirmed: [{
      Wallet: '0x49355...65241',
      Amount: '2.45 ETH',
      Date:'10/10/2017, 12:30 PM',
      Tokens: '245'
    },
    {
      Wallet: '0x49355...65241',
      Amount: '2.45 ETH',
      Date:'10/10/2017, 12:30 PM',
      Tokens: '245'
    },
    {
      Wallet: '0x49355...65241',
      Amount: '2.45 ETH',
      Date:'10/10/2017, 12:30 PM',
      Tokens: '245'
    },
  ]
  }, {
    firstName: "Joi",
    lastName : "Loin",
    Tel: "+ 00 097 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My rrr.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '513333 643',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'Joi@gmail.com',
    status: 'Refused'
  }, {
    firstName: "lari",
    lastName : "Fill",
    Tel: "+ 08 097 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My hh.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '513 643',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'lari@gmail.com',
    status: 'Refused',
    Confirmed: [{
      Wallet: '0x49355...65241',
      Amount: '2.45 ETH',
      Date:'10/10/2017, 12:30 PM',
      Tokens: '245'
    },
    {
      Wallet: '0x49355...65241',
      Amount: '2.45 ETH',
      Date:'10/10/2017, 12:30 PM',
      Tokens: '245'
    },
    {
      Wallet: '0x49355...65241',
      Amount: '2.45 ETH',
      Date:'10/10/2017, 12:30 PM',
      Tokens: '245'
    },
  ]
  },
  {
    firstName: "Netro",
    lastName : "Zetrov",
    Tel: "+ 34 097 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My pasport.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '513 643',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'Netro@gmail.com',
    status: 'Confirmed'
  }, {
    firstName: "Svan",
    lastName : "Aepa",
    Tel: "+ 35 097 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My pasport.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '113 643',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'Svan@gmail.com',
    status: 'Blocked'
  }, {
    firstName: "Coi",
    lastName : "Hoin",
    Tel: "+ 38 557 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My pasport.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '513 643',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'Coi@gmail.com',
    status: 'Confirmed'
  }, {
    firstName: "Mari",
    lastName : "Kill",
    Tel: "+ 38 044 333 44",
    Geo: '1751 Hotel Plaza Blvd, Lake Buena Vista, FL 32830, USA',
    passportID: 'My pasport.png',
    Manual: 'Manual Checking',
    adress:'0x78352...23566',
    Spark: '513 643',
    date: '10/11/18',
    addressDoc : "Passport or Government Issue of Card (front):",
    otherDoc: 'Passport or Government Issue of Card (back):',
  
    Email: 'Mari@gmail.com',
    status: 'Checking'
  }]
   prof = [...this.profiless];
   visible_table: boolean = true;
   visible_slide: boolean = false;
 
   grey_prew: boolean = false;
   grey_next: boolean = false;
   button_modal: boolean = false;
  
   menu_ = {
    Active: [ false, false, false , false ],
    active : [ true, false, false , false ],
    menu_search: [ ] ,
    search: [ ],
    right_menu: null ,
    input: '',
    num: 0
  };
 
   profile = [
    {
      ID:"2345",
      type:'Bay',
      address: "Kyiv",
      at: "Adress",
      amount: "Pull",
      Wallet_ID:'0x493...200',
      status:'Pending',
      data:'10/10/2017, 12:30 PM',
      Amount:"00000.0 OCG",
          hidden: false,
      lorem: "lorem lorem lorem lorem lorem lorem lorem lorem",
      price: "0.00000000 ETH",
      comments:true,
      tokens: "0440004#"
  
    }
    , {
      ID:"42345",
      type:'Bay',
      address: "Hello",
      at: "Adress",
      amount: "Pull",
      Wallet_ID:'0x493...200',
      status:'Pending',
      data:'10/10/2017, 12:30 PM',
      Amount:"00000.0 OCG",
          hidden: false,
      lorem: "lorem lorem lorem lorem lorem lorem lorem lorem",
      price: "0.00000000 ETH",
      comments:false,
      tokens: "0440004#"
    },
    {
      ID:"23458",
      type:'Sell',
      address: "Hello",
      at: "Adress",
      amount: "Pull",
      Wallet_ID:'0x493...200',
      status:'Pending',
      data:'10/10/2017, 12:30 PM',
      Amount:"00000.0 OCG",
          hidden: false,
      lorem: "lorem lorem lorem lorem lorem lorem lorem lorem",
      price: "0.00000000 ETH",
      comments:false,
      tokens: "0440004#"
    }
   ]
   
   input_search: string;
   menu_search_bar = {
    Active: [ false , false, false, false , false ],
    active : [ true, false, false , false, false ],
    menu_search: [ ] ,
    search: [ ]
  }
    // @Input('form-type') formType: 'login';



    counter: number = 0;
    prof_index: object;
    // = this.prof[this.counter];
    prev: boolean = false;
    next: boolean = false;
    see_investors = true;
   
    constructor() {

    }
    
    renovation(): void {
      this.prof_index = this.prof[this.counter];
      this.prev = this.counter === 0 ? true : false;
      this.next = this.counter ===  this.prof.length - 1 ? true : false;
     
    }
    move_slice(slice): void {
      if ( slice && this.counter !== this.prof.length -1  ) { 
        this.counter++ ;
        this.renovation();
        return
      }
      if ( !slice &&  this.counter > 0 ) {
       
        this.counter--;
        this.renovation();
        return
      }
      this.renovation();
   
    
    
    }
    see_investors_f(i): void {
      this.see_investors = !this.see_investors;
      if ( i ) { this.counter = i; }
      this.renovation();
    }
  //   check_arrows() {
  
  //     this.audit_array(false);
  //     this.audit_array(true);
  //   };
  
  
  //   search(date): void {
  //     const push_prof: Array<object> = [ ];
  //       for (let i = 0; i < this.prof.length; i++) {
  
  //         if ( ( this.prof[i].firstName + ' ' + this.prof[i].lastName + this.prof[i].Email ).match(   new RegExp( date , 'ig')   )  ) {
  //           push_prof.push(this.prof[i]);
  //         }
  //       }
  //      this.prof = [...push_prof]
  //   }
  
  // menu_saerch_active(num): void {
  //   this.menu_.active = [...this.menu_.Active];
  //   this.menu_.active[num] = true;
  //   this.menu_.num = num;
  //   }
  
  //   menu_search(date, num): void {
  
  //     if ( false !== num ) {
  //       this.menu_saerch_active(num);
  //       this.menu_.right_menu =  date;
  //       this.slice_second(false)
  //         if ( this.menu_.right_menu.target.outerText === 'All Investors' ) {
  
  //           if ( this.menu_.input.length < 0 ) { this.prof = [...this.profiless]; this.prof_index = this.prof[0]; return };
  //           this.prof = [...this.profiless];
  //           this.search(this.menu_.input);
  //           this.prof_index = this.prof[0];
  //           return
  //         }
  //         this.menu_.input.length < 0 ?   this.search(this.menu_.input) : this.prof = [...this.profiless];
  
  //                     const push_prof: Array<object> = [ ];
  
  //                     for (let i = 0; i < this.prof.length; i++) {
  //                       if (  this.prof[i].status === this.menu_.right_menu.target.outerText) {
  
  //                         push_prof.push( this.prof[i] );
  //                       }
  //                     }
  
  //                     this.prof = [...push_prof];
  //                     this.search(this.menu_.input);
  //                     this.slice_second(false);
  //                     if ( this.menu_.input.length < 0 ) {  return };
  //                     this.search(this.menu_.input);
  //                     this.prof_index = this.prof[0];
  //                     return
  //     }
  //       this.menu_.input = date;
  
  //        if ( date.length < 0 ) {  this.menu_search(this.menu_.right_menu, this.menu_.num); return   }
  //        this.menu_search(this.menu_.right_menu, this.menu_.num)
  
  //        this.search(this.menu_.input)
  //     }
  
  //  visible_function(i) {
  //     this.counter = i;
  //     this.prof_index = this.prof[i];
  //     this.hidden ( this.visible_slide);
  //     this.check_arrows();
  //  }
  // hidden ( e ): void {
  // if ( !e ) {
  //   this.visible_table = e;
  //   this.visible_slide = !e;
  //   return
  // }
  //   this.visible_table = e;
  //   this.visible_slide = !e;
  // }
  
  
  // slice_second(date): void {
  
  // if ( this.counter !== 0 && !date) {
  //   this.counter--
  // } else if ( this.counter !== this.prof.length - 1 && date) {
  //   this.counter++
  // }
  // this.check_arrows();
  // this.prof_index = this.prof[this.counter];
  // }
  
  
  // audit_array(date): void {
  //   if ( date ) {
  //     if (this.counter === this.prof.length - 1) {
  
  //       this.grey_next = true;
  //       return
  //     }
  //     this.grey_next = false;
  //     return
  //   }
  //     if (this.counter === 0 ) {
  
  //       this.grey_prew = true;
  //       return
  //     }
  //     this.grey_prew = false;
  
  // }
  
  
  // refusal_new(date): void {
  //   this.button_modal = date ?  true :  false;
  // }
 
    ngOnInit() {
      this.renovation();
      // document.getElementById('one_click').click();
      }

}
